---
title: "洛雪音乐助手 APP：全网公开音乐资源整合搜索与在线试听与下载"
slug: "Lx"
description: 洛雪音乐助手 APP：全网公开音乐资源整合搜索与在线试听与下载
date: 2022-07-04T14:45:20+08:00
categories: ["android","todo"]
tags: ["音乐","Github"]
---



## 简介

洛雪音乐助手，全网音乐聚合搜索、在线试听、下载，支持桌面歌词，不受限制。

[项目地址](https://github.com/lyswhut/lx-music-mobile)

[下载地址](https://github.com/lyswhut/lx-music-mobile/releases/download/v0.14.0/lx-music-mobile-v0.14.0-arm64-v8a.apk)

您也可以转到[本站分享网盘下载](https://list.1kbtool.com/)

演示：

![](2022-07-04-17-35-38.png)

## 使用说明

如前所述，本软件只是一个简单的客户端，最关键的还是数据源的问题。不同的数据源接口，支持的功能是完全不一样的。

我个人的配置如下图所示，仅供参考：

![](2022-07-04-17-36-22.png)

## 说明

软件转载自GitHub，所使用的接口均为互联网公开接口。

禁止在违反当地法律法规的情况下使用软件。本文作者不承担由此造成的任何直接、间接、特殊、偶然或结果性责任。

参考：

1. https://www.6yit.com/8498.html
2. https://github.com/lyswhut/lx-music-desktop
