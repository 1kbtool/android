---
title: "Nekogram：开源给力的第三方 电报（Telegram） 客户端（自带代理）"
description: "Nekogram：开源给力的第三方 电报（Telegram） 客户端（自带代理）"
slug: Nekogram
date: 2022-07-23T22:34:56+08:00
draft: false
categories: ["android"]
tags: ["Github","第三方"]
---

## 简介

Nekogram 是一个第三方 Telegram 客户端。其特点是支持多账号切换，以及自带公共代理功能，无需另外寻找梯子，对小白非常友好。

### 相关链接

- 项目地址：[GitHub - NekoX-Dev/NekoX: A third-party Telegram android app.](https://github.com/NekoX-Dev/NekoX)
- 下载链接：[Getting Title at 32:40](https://github.com/NekoX-Dev/NekoX/releases/download/v8.8.5-rc01/NekoX-v8.8.5-rc01-full-arm64-v8a-release.apk)
- 也可以转到[本站分享网盘下载](https://list.1kbtool.com/)

## 使用教程

电报首次注册需要验证手机，直接使用国内手机号码验证即可。注意提前开启公共代理。

![](./2022-07-23-22-27-50.png)

![](./2022-07-23-22-28-19.png)

在菜单最下方也可以开启代理。

## 附录

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。