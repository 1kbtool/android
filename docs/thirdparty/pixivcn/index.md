---
title: "Shaft：安卓Pixiv第三方客户端，无广告，功能增强"
date: 2022-02-10T11:33:07+08:00
draft: false
slug: Pixivcn
categories: ["android","todo"]
tags: ["Github","第三方"]
---

Shaft (Pixiv 第三方客户端)。本应用为日本插画交流网站Pixiv的安卓客户端第三方重制版，项目已开源且仅做交流和学习使用，不得用于任何商业用途，APP内所有插画、漫画、小说作品版权均归属于其著作者或Pixiv所有。

功能：无广告，支持直连、多用户、非会员按热度排序、批量下载等。

[项目地址](https://github.com/CeuiLiSA/Pixiv-Shaft)

[下载地址](https://github.com/CeuiLiSA/Pixiv-Shaft/releases/download/v3.2.21/PixShaft_3.2.21.apk)

[备用下载](http://tmp.link/f/62048adc23ef9)

>类似的项目有[PixEz](https://github.com/Notsfsssf/Pix-EzViewer)，但此项目长期未更新release，并且本人尝试多次均无法登录，故不作推荐。

参考：

1. https://awslnotbad.oschina.io/2020/11/23/Pixiv/  Pixiv第三方
2. https://zhuanlan.zhihu.com/p/447086383  PixEz介绍
