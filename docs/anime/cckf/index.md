---
title: "纯纯看番：安卓端追番神器，内置播放源"
description: "纯纯看番：安卓端追番神器，内置播放源"
slug: Cckf
date: 2022-07-11T14:43:05+08:00
draft: false
tags: ['']
categories: ['']
---

## 简介

纯纯看番，一个追番看番神器，目前支持Android版，内置4个番剧源，樱花动漫、哔咪动漫 Bimibimi、AGE 动漫和森之屋动漫，多番剧源任意切换，使用无需登录，免费无广告，安装完打开直接看，支持黑暗模式。

![](2022-07-11-14-55-52.png)

![](2022-07-11-14-56-00.png)


### 相关链接

- 项目地址：[GitHub - heyanLE/EasyBangumi: 纯纯看番，追番和看番的安卓软件，多番剧源 -樱花动漫 -Bimibimi - AGE 动漫](https://github.com/heyanLE/EasyBangumi)
- 下载链接：[Getting Title at 52:50](https://github.com/heyanLE/EasyBangumi/releases/download/2.0.2/2.0.2.apk)
- 也可以转到[本站分享网盘下载](https://list.1kbtool.com/)

## 使用教程

直接打开使用即可。不同源更新进度也不一样。个人感觉比较好用的是樱花源。

## 小编吐槽

@royse

人间自有真情在

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

