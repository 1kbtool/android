---
title: "Google框架"
date: 2022-03-01T17:20:46+08:00
draft: true
slug: Googleservice
categories: ["android","todo"]
---

在我们下载国外软件时通常需要谷歌框架的支持，但是国内手机往往默认不开启谷歌框架。部分手机可以通过手机设置进行打开，例如我的redmi k30 pro。而部分手机直接默认没有谷歌框架，或者需要通过刷机Root等操作进行打开

可以考虑使用appmirror

参考：
