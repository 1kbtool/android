---
title: "See：轻量无广告的安卓微博第三方客户端"
date: 2022-05-23T10:54:56+08:00
draft: false
slug: Weibolite
categories: ["android","todo"]
tags: ["第三方"]
---

## 简介

See微博是一个轻量、简单、无广告的第三方微博客户端。不仅支持微博大部分的基础功能，而且支持一键下载原图、修改机型、悬浮视频、话题自动一键签到等黑科技，另外支持屏蔽词、各种好用的手势，保证用起来就是爽。

### 相关链接

[官方网站](https://caij.xyz/)

[下载地址](https://caij.coding.net/p/datacenter/d/datacenter/git/raw/master/update/com-caij-see-publish-release-2.0.5.4-309-shengji.apk)

![](2022-02-10-11-28-14.png)

## 使用教程

下载安装即可

微博的第三方客户端也是非常多的。这里我只介绍See这一款客户端。

本软件不开源，但是开发者网站备了案，应该是没毒的。介意者勿用。

支持手机号登录，反应有点慢，第三方软件通病了。

## 小编吐槽

@royse

其实我感觉在一堆国产“3A”客户端中，最需要优化一下的就是微博了。反正每次我打开微博，他都会弹出一堆窗口让我关注某某博主。真的很烦。

## 附录

### Reference

1. https://www.v1tx.com/post/best-weibo-apps/  几个第三方客户端介绍
2. https://zhuanlan.zhihu.com/p/392861770  关于Share


### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

