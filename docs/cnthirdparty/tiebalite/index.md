---
title: "贴吧lite 4.0：轻量无广告的安卓贴吧第三方客户端，安装包仅7MB"
description: "贴吧lite 4.0：轻量无广告的安卓贴吧第三方客户端，仅7MB"
date: 2022-06-23T09:54:57+08:00
draft: false
slug: Tiebalite
categories: ["android"]
tags: ["Github","第三方"]
---

## 简介

贴吧 Lite 是一个**非官方**的贴吧客户端。本软件及源码仅供学习交流使用，严禁用于商业用途。

贴吧官方客户端广告比较多，有些影响体验。贴吧Lite是一款开源免费的贴吧客户端，软件精简（仅4.7MB），界面清爽，最重要的是没有广告。

4.0 版本已在电报发布。

![](2022-07-11-15-06-00.png)

### 相关链接

- 原 [项目地址](https://github.com/HuanCheng65/TiebaLite) （作者目前不在 Github 更新）
- 通知频道/新版下载 [贴吧 Lite 通知频道🔈](https://t.me/tblite)（需要代理才能打开）
- 您也可以转到[本站分享网盘下载](https://list.1kbtool.com/)

## 使用教程

下载安装即可，**第一次登录验证比较慢，如果卡在图片验证码可以退出重进**，之后看贴加载速度还是可以的。

## 小编吐槽

@royse

没有广告，赞一个。不过，不太稳定也是真的。


## 附录

### Reference

1. https://github.com/HuanCheng65/TiebaLite
2. https://www.bilibili.com/read/cv5762909  一个介绍

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。