---
title: "阅读3.0：简洁美观开源的全网小说免费阅读安卓 APP"
slug: Read3
date: 2022-07-10T13:57:54+08:00
draft: false
---
## 简介

阅读3.0是一款可以**自定义来源**阅读网络内容的工具。只要能找到优质书源，理论上可以阅读任意内容。

![](2022-07-11-13-07-10.png)

![](2022-07-11-13-07-02.png)

### 相关链接

- 项目地址：[GitHub - gedoor/legado: 阅读3.0, 阅读是一款可以自定义来源阅读网络内容的工具，为广大网络文学爱好者提供一种方便、快捷舒适的试读体验。](https://github.com/gedoor/legado)
- 软件本体下载链接：[legado_app_3.22.071010.apk](https://github.com/gedoor/legado/releases/download/3.22.071010/legado_app_3.22.071010.apk)
- 备用下载：也可以转到[本站分享网盘下载](https://list.1kbtool.com/)
- 优质书源项目地址：[GitHub - XIU2/Yuedu: 📚「阅读」APP 精品书源（网络小说）](https://github.com/XIU2/Yuedu)
- 书源：[https://yuedu.xiu2.xyz/shuyuan](https://yuedu.xiu2.xyz/shuyuan)以及[raw](https://raw.xn--p8jhe.tw/XIU2/Yuedu/master/shuyuan)

## 使用教程

需要导入书源。下载安装软件之后，打开「阅读」APP点击右下角的 [我的] 按钮 - [书源管理]，这时候再点击右上角的 [三圆点] 按钮 - [二维码导入](最下方第二张图) - 然后手机扫描下方二维码即可即可。

![](2022-07-11-12-56-51.png)

如这一阅读源无法满足您的需求，您也使用 


## 小编吐槽

@royse

有些书源用不了，挺不爽的。自己挑着用吧。然后还有点乱码。

>开发者版本：服务器部署阅读3.[【好玩儿的Docker项目】 10分钟搭建一个自己的网文阅读器 安卓的“阅读”APP网页版 附带书源！ – 我不是咕咕鸽——VPS折腾不完全记录](https://blog.laoda.de/archives/docker-compose-install-reader)

## 附录

### Reference

<!-- 参考的文章 1 -->

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

