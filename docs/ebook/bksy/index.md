---
title: "博看书苑：可以免费阅读正版电子图书、期刊、报纸，总存储量 21TB"
slug: Bksy
date: 2022-07-10T18:06:31+08:00
draft: false
---

## 简介

博看书苑：可以免费阅读正版电子图书、期刊、报纸，总存储量 21TB。

官网下载：[博看书苑APP](https://download.bookan.com.cn)

![](2022-07-11-12-26-09.png)

![](2022-07-11-12-26-15.png)

## 使用说明

第一次注册，APP 会要求用户填写机构授权码，每个机构码内包含的书籍略有不同。

最新可用机构码

```
浙江图书馆：zjlib
上海图书馆：shlib
山东图书馆：sdlib
山西图书馆：sxlib
甘肃图书馆：gzlib
新疆图书馆：xjlib
武汉图书馆：whlib
随州图书馆：szlib
镇江图书馆：zjst
梧州图书馆：wzlib
武汉大学图书馆：whu
吉林大学图书馆：jlulib
四川大学图书馆：scu
河南大学图书馆：hndx
衢州学院图书馆：qzxylib
海南热带海洋学院：qzulib
中国药科大学图书馆：cpuedu
广西壮族自治区图书馆：gxlib
四川图书馆：sclib
安微省图书馆账号：ahst
营口理工学院账号：yklgxy
南华大学图书馆账号：usc
大连大学图书馆账号：DALIANDX
兴义市图书馆账号xyslib
对外经济贸易大学图书馆账号：uibelib
东北农业大学图书馆账号：dbnylib
成都中医院大学图书馆账号：cdzyytsg
大连教育学院图书馆账号：dljyxy
广东省立中山图书馆账号：gdzslib
广西科技大学鹿山学院图书馆账号：lsxylib
广西大学图书馆账号：gxulib
河南建筑职业技术学院账号：hnjzzy
云南师范大学图书馆账号：ynnulib
广州图书馆账号：gzst密码：gzst
重庆人文科技学院图书馆账号：cqrwxy
首都医科大学图书馆账号：cpums
沈阳大学图书馆账号：sydxlib
湛江幼儿师范专科学校账号：zjyesf
```


## 小编吐槽

@royse

挺好用的，看看期刊什么的。

## 附录

### Reference


### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

