---
title: "Aria2app：手机上的全功能下载工具，替代迅雷"
date: 2022-02-11T10:15:23+08:00
draft: false
slug: Aria2app
categories: ["android","todo"]
tags: ["资源嗅探","BT下载","安卓","Aria2","GEEK"]
---

## 简介

如果你对BT下载有一定了解，一定听说过Aria2。Aria2是目前最流行的下载工具，支持几乎所有资源，自动多线程，下载速度很快。并且具有很好的拓展性，支持远程下载等。

Aria2App是一款安卓平台的开源、免费、无广告的全能下载工具，支持下载 HTTP、FTP、BT、磁力链等资源。内置浏览器直接下载，**支持中文。**

### 下载

[下载地址](https://github.com/devgianlu/Aria2App/releases/download/v5.9.9/app-foss-release.apk)

[备用下载地址](http://tmp.link/f/6205d1f3a83e6)

![](2022-02-11-11-03-56.png)

![](2022-02-11-11-04-03.png)

![](2022-02-11-11-04-08.png)

稍微有些不方便的是，文件下载完成后不会直接打开，需要转到手机的文件管理软件，到 下载 文件夹（/Download）查看。

更深入地说，aria2app其实是一款aria2服务器管理工具，可以便捷的管理部署了aria2内核的服务器实例。我们这里只是调用了内置的实例，如果家里有NAS等，也可以直接用这个软件进行管理。

功能：

- 同时处理更多服务器
- 添加 HTTP(s)、(s)FTP、BitTorrent、 Metalink 下载
- 通过集成搜索引擎添加 Torrent
- 显示下载中每个文件的信息
- 通过 DirectDownload 把文件从服务器下载到您的设备

参考：

1. https://github.com/devgianlu/Aria2App/releases/tag/v5.9.9
