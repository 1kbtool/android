---
title: "1DM/IDM+：全能下载器+资源嗅探"
date: 2022-02-11T11:22:26+08:00
draft: false
slug: 1dmplus
categories: ["android","todo"]
tags: ["资源嗅探","BT下载","安卓"]
---

## 简介

1DM+号称是目前 Android 平台最快、最先进的下载管理器应用，支持通过Torrent下载，以及浏览器资源嗅探。官方号称其下载速度是正常下载速度的 500％。实际下载速度也是很可观的，可以和ADM比肩。而且 IDM+ 不运行后台服务，如果没有什么下载和智能下载选项被禁用这增加了电池的寿命。

1DM有广告，免费，1DM+无广告，正版需购买。

现在官方版本已经到了15.2。可在Google商店下载。

截图：

![](2022-02-22-17-31-16.png)

![](2022-02-22-17-31-22.png)

### 下载

[APKMirror 15.2 1DM](https://www.apkmirror.com/apk/vicky-bonick/idm-download-video-movie-music-anime-torrent/idm-download-video-movie-music-anime-torrent-15-2-release/)

[酷安 12.3 1DM+](https://www.coolapk.com/apk/idm.internet.download.manager.plus)

[备用下载链接](http://tmp.link/f/6214a79985f52)

## 使用教程



参考：

1. https://www.yxssp.com/23740.html
2. https://play.google.com/store/apps/details?id=idm.internet.download.manager.plus&hl=en_US&gl=US  谷歌官方版
3. https://www.coolapk.com/apk/idm.internet.download.manager.plus  酷安旧版
