---
title: "Apkmirror：快速安装国外软件"
date: 2022-02-22T16:59:20+08:00
draft: false
slug: Apkmirror
categories: ["android","todo"]
tags: ["安卓","应用下载"]
---

## 简介

现在国产手机有一个很明显的去谷歌化的趋势。这一方面提高了国产手机的自主性，但另一方面给我们安装国外软件带来了许多不便。

许多手机即使能够访问谷歌，也没办法安装谷歌框架，Google Play Store打开闪退。

而apkmirror上包括了许多常用软件的Google商店镜像，网页直接下载安装。

![](2022-02-22-17-06-14.png)

[官网](https://www.apkmirror.com/)

## 使用教程

apkmirror只有网页版，没有手机客户端，并且下载时往往有多次跳转选择，故在使用时稍显繁琐。

从apkmirror下载的软件基本是apk格式，很少有xapk新格式。这一点比较方便。

## 吐槽

@royse

相信大家听说的比较多的另外一家Google Play镜像服务就是apkpure了。但是据传apkpure是一家中国公司，且在其平台上下载的软件中会附有木马，具有一定的危险性。故这里并不是很推荐。

