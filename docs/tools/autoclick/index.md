---
title: "Autoclicker：自动连点器"
date: 2022-03-06T20:30:43+08:00
draft: false
slug: Autoclick
categories: ["android","todo"]
tags: ["Github","实用工具","GEEK"]
---

一款开源的自动连点工具，可自动检测状况改变实现点击。安卓平台。功能比较简陋，似乎不能设置停止时间，运行后会导致手机比较卡。不过开发者还没有停止开发，期待之后更好的结果。

Smart AutoClicker is an Android application allowing to automate repetitive task by clicking automatically for you on the screen. Unlike the regular auto clicker application, the clicks aren't based on timers to execute the clicks. Instead, it allows you to capture an image from a part of your screen and execute the click once this image is detected again.

[GitHub项目地址](https://github.com/Nain57/Smart-AutoClicker)

[下载地址](https://github.com/Nain57/Smart-AutoClicker/releases/download/1.2.4/smartautoclicker-release.apk)

备用下载地址

![](2022-03-06-20-35-26.png)

![](2022-03-06-20-35-37.png)

参考：

1. https://www.appinn.com/zidongdianjiqi-for-android/  另一款连点工具，功能稍微强大一些，但是似乎已经下架了