---
title: "安卓消息转发软件"
date: 2022-03-15T19:48:48+08:00
draft: false
slug: Msgforward
categories: ["android","todo"]
tags: ["Github","实用工具","GEEK"]
---

[项目地址](https://github.com/pppscn/SmsForwarder)

短信转发器——监控Android手机短信、来电、APP通知，并根据指定规则转发到其他手机：钉钉机器人、企业微信群机器人、飞书机器人、企业微信应用消息、邮箱、bark、webhook、Telegram机器人、Server酱、PushPlus、手机短信等。

⚠ 首发地址：https://github.com/pppscn/SmsForwarder/releases

⚠ 国内镜像：https://gitee.com/pp/SmsForwarder/releases

⚠ 网盘下载：https://wws.lanzoui.com/b025yl86h 访问密码：pppscn

⚠ 酷安应用市场：https://www.coolapk.com/apk/com.idormy.sms.forwarder

使用文档
⚠ 首发地址：https://github.com/pppscn/SmsForwarder/wiki

⚠ 国内镜像：https://gitee.com/pp/SmsForwarder/wikis/pages

参考：
