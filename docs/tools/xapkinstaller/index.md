---
title: "Xapk是什么？如何安装？"
date: 2022-02-15T15:53:26+08:00
draft: false
slug: Xapkinstaller
categories: ["android","todo"]
tags: ["实用工具"]
---

## 是什么？

Xapk是2016年左右新推出的安装包格式，旨在整合apk安装包与obb数据缓存。目前从谷歌商店以及国外平台上下载的最新应用有些会采取此格式。

## 如何安装？

使用xapk installer进行管理和安装

[apkfab](https://apkfab.com/xapk-manager/com.apkfab.installer/download)

[备用下载地址](http://tmp.link/f/620b5d8daccf7)

[备用](http://tmp.link/f/620b5ebd8ce46)

或是直接通过应用市场软件进行安装

参考：
